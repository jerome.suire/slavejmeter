FROM java
RUN wget https://www-eu.apache.org/dist//jmeter/binaries/apache-jmeter-5.1.1.tgz
RUN tar xzf apache-jmeter-5.1.1.tgz
COPY rmi_keystore.jks apache-jmeter-5.1.1/bin/
EXPOSE 1099 4445
WORKDIR apache-jmeter-5.1.1/bin/
ENTRYPOINT ["./jmeter-server"]
